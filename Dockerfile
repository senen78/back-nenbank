FROM node

WORKDIR /nenbank

ADD . /nenbank

EXPOSE 3000

CMD ["npm","start"]