const config = require('../config-module.js').config();
const requestJson = require('request-json');
var logger = require('../gestionLog').logger;

var emailvalidator = require('regex-email');
var passwordValidator = require('password-validator');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

// Create a schema
var schema = new passwordValidator();
 
// Add properties to it
schema
.is().min(4)                                    // Minimum length 4
.is().max(8)                                  // Maximum length 8
.has().letters()                              // Must have letters
.has().digits()                                 // Must have digits
.has().not().spaces()                           // Should not have spaces
.is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values

/** @description Login del usuario.  
 */
 function doLogin(req, res, next) {

 	logger.info("---------> DO LOGIN" );

 	var query = 'q={"email":"' + req.body.email +'"}';

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("user?" + query + "&" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (!body[0]) return res.status(404).send({"msg":'Email incorrecto'});
		    var passwordIsValid = bcrypt.compareSync(req.body.password, body[0].password);
		    if (!passwordIsValid) return res.status(401).send({ auth: false, token: null, msg: "Password inválida" });
		    var token = jwt.sign({ id: body[0]._id }, config.secret, {
		      expiresIn: 3600
		    });
		    console.log(body[0]);
		    var user = JSON.stringify(body[0]);
		    res.status(200).send({ "auth": true, 
		    	"token": token,
		    	"user": user
		    });
		}
		)
 }


/** @description Logout del usuario  
 */
 function doLogout(req, res, next) {

 	logger.info("---------> DO LOGOUT" );
 	res.status(200).send({ auth: false, token: null });
 }



/** @description Registro del usuario
 */
 function doRegistry(req, res, next) {

 	logger.info("---------> DO REGISTRY" );

	var name = req.body.first_name
	var surname = req.body.last_name
 	var email = req.body.email
 	var password = req.body.password

 	var validData = name && surname && emailvalidator.test(email) && schema.validate(password);

 	if (!validData) return res.status(500).send({"msg":"Datos introducidos no válidos"});

	var hashedPassword = bcrypt.hashSync(req.body.password, 8);
	var postBody = '{"first_name":"' + name + '", "last_name":"' + surname + '", "email":"' + email + '","password":"' + hashedPassword + '", "accounts":[]}';

 	var httpClient = requestJson.createClient(config.baseMLabURL);

	 httpClient.post("user?"+ config.mLabAPIKey, JSON.parse(postBody), 
	 		function(err, resMLab, body){	
	 			if (err || body.message) return res.status(500).send({"msg": body.message.slice(0, 40) || 'Error en el servidor'});
			    var token = jwt.sign({ id: body._id }, config.secret, {
      				expiresIn: 3600 
  				});
		    res.status(200).send({ "auth": true, 
		    	"token": token,
		    	"user": body
		    });		
	 		}
	 	); 	
 }



 module.exports = {
 	doLogin,
 	doLogout,
 	doRegistry
 }