const config = require('../config-module.js').config();
const requestJson = require('request-json');
var logger = require('../gestionLog').logger;

var moment = require('moment');
var user = require('../controllers/userController');

/** @description Devuelve la lista de cuentas. 
 */
 function getAccounts(req, res, next) {

 	logger.debug("---------> GET ACCOUNTS" );

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("account?" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (!body[0]) return res.status(404).send({"msg":'No hay cuentas en el sistema'});
 			res.status(200).send(body);
 		}
 	);	
 }

 /** @description Devuelve la cuenta por ID. 
 */
 function getAccount(req, res, next) {

 	var id = req.params.id;

 	logger.debug("---------> GET ACCOUNT" );
 	logger.info("Acceso a los datos de la cuenta con ID: " +  id);

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("account/" + id + "?" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Cuenta no encontrada'});
 			res.status(200).send(body);	
 		}
 		);	
 }

 /** @description Devuelve la cuenta por IBAN.  
 */
 function getAccountByIBAN(req, res, next) {

 	var iban = req.params.iban;

 	logger.debug("---------> GET ACCOUNT BY IBAN" );
 	logger.info("Acceso a los datos de la cuenta con IBAN: " +  iban);

 	var query = 'q={"IBAN":"' + iban + '"}';

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("account?" + query + "&" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Cuenta no encontrada'});
 			res.status(200).send(body);	
 		}
 		);	
 }

/** @description Añade una cuenta.   
 */
 function createAccount(req, res, next) {

 	logger.debug("---------> ADD ACCOUNT" );

 	var iban = req.body.iban
 	var iduser = req.body.iduser

 	if (!(iban && iduser)) return res.status(500).send({"msg":'Datos incorrectos'});

 	var postBody = '{"IBAN":"' + iban + '", "date":"' + moment().format('L') + '", "balance":' + 0.00 + ', "movements":[]}';

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.post("account?"+ config.mLabAPIKey, JSON.parse(postBody),  
 		function(err, resMLab, body){
	 		if (err || body.message) return res.status(500).send({"msg": body.message.slice(0, 40) || 'Error en el servidor'});
	 		user.addAccountToUser(iduser, iban);  //Se necesitaría una promesa para comprobar el correcto funcionamiento??
 			res.status(200).send(body);	
 		}
 		);
 }


 /** @description Borra una cuenta por ID.  
 */
 function dropAccount(req, res, next) {

 	var id = req.params.id;

 	logger.debug("---------> DROP ACCOUNT" );
 	logger.info("Acceso a los datos de la cuenta con ID: " +  id);

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.delete("account/" + id + "?" + config.mLabAPIKey, 
 		function(err, resMLab, body){	
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Cuenta no encontrada'});
 			res.status(200).send({"msg":'Cuenta borrada correctamente'});
 			//Falta borrar la cuenta del usuario
 		}
 		);	
 }

/** @description Añade un movimiento a la lista y recalcula el balance de la cuenta
 */
 function addMovement(req, res, next) {

 	var iban = req.params.iban;

 	logger.debug("---------> DROP ACCOUNT" );
 	logger.info("Acceso a los datos de la cuenta con IBAN: " +  iban);

 	var amount = req.body.amount;
 	var category = req.body.category;

 	var movement = {
 		"amount" : amount, 
 		"date" : moment().format('L'), 
 		"time" : moment().format('HH:MM:SS.SSS'), 
 		"category" : category
 	}

 	var putBody = '{"$inc":{"balance":'+amount+'},"$push":{"movements":'+JSON.stringify(movement)+'}}';
 	var query = 'q={"IBAN":"' + iban + '"}';

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.put("account?" + query + "&" + config.mLabAPIKey, JSON.parse(putBody), 
 		function(err, resMLab, body){	
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
 			console.log(body);
		    if (body.message) return res.status(404).send({"msg":'Cuenta no encontrada'});
 			res.status(200).send({"msg":'Movimiento apuntado correctamente'});
 		}
 		);
 }

 module.exports = {
 	getAccounts,
 	getAccount,
 	getAccountByIBAN,
 	createAccount,
 	dropAccount,
 	addMovement
 }