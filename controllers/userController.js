const config = require('../config-module.js').config();
const requestJson = require('request-json');
var logger = require('../gestionLog').logger;

var emailvalidator = require('regex-email');
var passwordValidator = require('password-validator');
var bcrypt = require('bcryptjs');

var schema = new passwordValidator();

// Password requisites
schema
.is().min(4)                                    // Minimum length 4
.is().max(8)                                  // Maximum length 8
.has().letters()                              // Must have letters
.has().digits()                                 // Must have digits
.has().not().spaces()                           // Should not have spaces
.is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values

/** @description Devuelve la lista de usuarios. 
 */
 function getUsers(req, res, next) {

 	logger.debug("---------> GET USERS" );

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("user?" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (!body[0]) return res.status(404).send({"msg":'No hay usuarios en el sistema'});
 			res.status(200).send(body);
 		}
 	);	
 }

/** @description Devuelve el usuario por ID. 
 */
 function getUser(req, res, next) {

 	var id = req.params.id;

 	logger.debug("---------> GET USER" );
 	logger.info("Acceso a los datos del usuario con ID: " +  id);

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("user/" + id + "?" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Usuario no encontrado'});
 			res.status(200).send(body);	
 		}
 		);	
 }

/** @description Devuelve el usuario pmediante el token 
 */
 function getUserToken(req, res, next) {
 	var id = req.userId.$oid;

 	logger.info("---------> GET USER TOKEN" );
 	logger.info("Acceso a los datos del usuario con ID: " +  id);

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.get("user/" + id + "?" + config.mLabAPIKey, 
 		function(err, resMLab, body){
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Usuario no encontrado'});
 			res.status(200).send(body);	
 		}
 		);	
 }

 /** @description Borra el usuario por ID.  
 */
 function dropUser(req, res, next) {

 	var id = req.params.id;

 	logger.debug("---------> DROP USER" );
 	logger.info("Acceso a los datos del usuario con ID: " +  id);

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.delete("user/" + id + "?" + config.mLabAPIKey, 
 		function(err, resMLab, body){	
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Usuario no encontrado'});
 			res.status(200).send({"msg":'Usuario borrado correctamente'});
 		}
 		);	
 }

/** @description Actualiza los detalles del usuario
 */
 function updateUser(req, res, next) {

 	var id = req.params.id;

 	logger.debug("---------> UPDATE USER" );
 	logger.info("Acceso a los datos del usuario con ID: " +  id);

 	var name = req.body.first_name;
 	var surname = req.body.last_name;
 	var email = req.body.email;
 	var password = req.body.password;
 	var errorData ="";

 	var user = '{"$set":{'

 	if (name) user += '"first_name":"' + name +'",';
 	if (surname) user += '"last_name":"' + surname +'",';

 	if (email) {
 		if (emailvalidator.test(email)) {
 			user += '"email":"' + email +'",';
 		}
	 	else {						
	 		errorData = {"msg" : "Email no válido"};
	 	}
	}

 	if (password) {
 		if (schema.validate(password)) {
 			var hashedPassword = bcrypt.hashSync(req.body.password, 8);
 			user += '"password":"' + hashedPassword +'",';
 		}
	 	else {						
	 		errorData = {"msg" : "Email no válido"};
	 	}
	}

 	user = user.slice(0,-1);
 	user += '}}';

 	if (errorData) res.status(500).send(errorData);

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.put("user/" + id + "?" + config.mLabAPIKey, JSON.parse(user), 
 		function(err, resMLab, body){	
 			if (err) return res.status(500).send({"msg":'Error en el servidor'});
		    if (body.message) return res.status(404).send({"msg":'Usuario no encontrado'});
 			res.status(200).send({"msg":'Usuario actualizado correctamente'});
 		}
 		);
 }
 

 function addAccountToUser(iduser,iban) {

 	logger.debug("---------> UPDATE USER ACCOUNTS" );
 	logger.info("Acceso a los datos del usuario con ID: " +  iduser);

 	var putBody = '{"$push":{"accounts":"'+iban+'"}}';  

 	var httpClient = requestJson.createClient(config.baseMLabURL);

 	httpClient.put("user/" + iduser + "?" + config.mLabAPIKey, JSON.parse(putBody), 
 		function(err, resMLab, body){	
 			if (err) response = {"msg":'Error en el servidor'};
		    if (body.message) response = {"msg":'Usuario no encontrado'};
 			response = {"msg":'Usuario actualizado correctamente'};
 			console.log(response);
 			return response
 		}
 		);
 }

 module.exports = {
 	getUsers,
 	getUser,
 	getUserToken,
 	updateUser,
 	dropUser,
 	addAccountToUser
 }