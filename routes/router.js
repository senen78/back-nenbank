'use strict'

var express = require('express');
var VerifyToken = require('../VerifyToken');
var api = express.Router();

var user = require('../controllers/userController');
var access = require('../controllers/accessController');
var account = require('../controllers/accountController');

api.get('/v1/user', VerifyToken, user.getUsers);
api.get('/v1/user/token', VerifyToken, user.getUserToken);
api.get('/v1/user/:id', VerifyToken, user.getUser);
api.put('/v1/user/:id', VerifyToken, user.updateUser);
api.delete('/v1/user/:id', VerifyToken, user.dropUser);

api.get('/v1/account', VerifyToken, account.getAccounts);
api.get('/v1/account/:id', VerifyToken, account.getAccount);
api.get('/v1/account/iban/:iban', VerifyToken, account.getAccountByIBAN);
api.post('/v1/account', VerifyToken, account.createAccount);
api.delete('/v1/account/:id', VerifyToken, account.dropAccount);

api.post('/v1/account/iban/:iban/movement', VerifyToken, account.addMovement);

api.post('/v1/login', access.doLogin);
api.post('/v1/logout',access.doLogout);
api.post('/v1/registry', access.doRegistry);



module.exports = api;
