'use strict'

const config = require('./config-module.js').config();
const logger = require('./gestionLog').logger;

var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var app = express();
var api = require('./routes/router');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use((req, res, next) =>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers',
				'x-access-token, X-API-KEY, Origin, X-Requested-With, Content-type, Accept, Access-Control-Request-Method');
	res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');

	next();
})

app.use('/nenbank', api);

app.get('/', (req, res) => res.send('Server UP!! Utiliza el API /nenbank'));

app.get('/status', (req, res) => res.send('App UP!!'));

app.get('*', (req, res) => res.send('Aquí no hay nada!!'));

app.listen(config.PORT, ()=> {
  	logger.info('API NeN Bank escuchando en el puerto: '+config.PORT);
});

module.exports = app;